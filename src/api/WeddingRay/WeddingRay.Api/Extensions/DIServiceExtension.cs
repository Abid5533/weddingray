﻿using Microsoft.EntityFrameworkCore;
using WeddingRay.DAL.Entities;
using WeddingRay.DAL.Repository.Implementations;
using WeddingRay.DAL.Repository.Interfaces;
using WeddingRay.Factory.Implementations;
using WeddingRay.Factory.Interface;
using WeddingRay.Service.Implementations;
using WeddingRay.Service.Interface;

namespace WeddingRay.Api.Extensions
{
    public static class DIServiceExtension
    {
        public static IServiceCollection AddDatabase(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<WeddingRayEngineContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("WeddingRayDB"));
            }
            );
            return services;
        }

        public static IServiceCollection AddRepositories(this IServiceCollection services, IConfiguration configuration)
        {
            return services
                .AddScoped<IApplicationConfigurationManager, ApplicationConfigurationManager>()
                .AddScoped<IAlbumRepository, AlbumRepository>()
                .AddScoped<IAlbumDetailsRepository, AlbumDetailsRepository>();
        }
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            return services
                .AddScoped<IAlbumService, AlbumService>()
                .AddScoped<IAlbumDetailsService, AlbumDetailsService>();
        }
        public static IServiceCollection AddFactories(this IServiceCollection services)
        {
            return services
                .AddScoped<IAlbumFactory, AlbumFactory>()
                .AddScoped<IAlbumDetailsFactory, AlbumDetailsFactory>();
        }
    }
}
