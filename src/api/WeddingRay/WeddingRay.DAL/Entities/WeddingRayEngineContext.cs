﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeddingRay.DAL.Entities
{
    public partial class WeddingRayEngineContext : DbContext
    {
        public WeddingRayEngineContext()
        {

        }

        public WeddingRayEngineContext(DbContextOptions<WeddingRayEngineContext> options)
          : base(options)
        {
        }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Name=WeddingRayDB");
            }
        }


        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
