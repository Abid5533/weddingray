﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeddingRay.DAL.Entities
{
    public class Album
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Order { get; set; }
        public byte[] Picture { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
    }
}
