﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeddingRay.DAL.Entities
{
    public class AlbumDetails
    {
        public int Id { get; set; }
        public int AlbumId { get; set; }
        public byte[] Picture { get; set; }
        public int Order { get; set; }
        public virtual Album Album { get; set; } = null!;
    }
}
