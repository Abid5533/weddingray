﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeddingRay.DAL.Entities;
using WeddingRay.DAL.Repository.Interfaces;

namespace WeddingRay.DAL.Repository.Implementations
{
    public class AlbumDetailsRepository : IAlbumDetailsRepository
    {
        private readonly DbContextOptionsBuilder<WeddingRayEngineContext> _dbContextOptionBuilder;

        public AlbumDetailsRepository(IApplicationConfigurationManager applicationConfigurationManager)
        {
            _dbContextOptionBuilder = new DbContextOptionsBuilder<WeddingRayEngineContext>();
            _dbContextOptionBuilder.UseSqlServer(applicationConfigurationManager.GetConnectionString());

        }
    }
}
