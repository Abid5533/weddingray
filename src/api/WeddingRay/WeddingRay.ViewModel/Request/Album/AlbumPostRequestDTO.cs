﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeddingRay.ViewModel.Request.Album
{
    public record AlbumPostRequestDTO : BasePostRequestDTO
    {
        public AlbumPostRequestDTO(int Id) : base(Id)
        {

        }

        public string Name { get; set; } 
        public int Order { get; set; } 
        public byte[] Picture { get; set; }
    }
}
