﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeddingRay.ViewModel.Request.AlbumDetails
{
    public record AlbumDetailsPostRequestDTO : BasePostRequestDTO
    {
        public AlbumDetailsPostRequestDTO(int Id):base(Id)
        {

        }
    }
}
