﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeddingRay.ViewModel.Response.Album
{
    public record  AlbumResponseDTO : BaseGetResponseDTO
    {
        public AlbumResponseDTO(int Id):base(Id)
        {

        }
    }
}
