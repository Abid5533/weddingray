﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeddingRay.ViewModel.Response
{
    public abstract record BaseGetResponseDTO(long Id);
}
