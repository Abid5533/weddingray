﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeddingRay.ViewModel.Response.AlbumDetails
{
    public record AlbumDetailsResponseDTO : BaseGetResponseDTO
    {
        public AlbumDetailsResponseDTO(int Id):base(Id)
        {

        }
    }
}
