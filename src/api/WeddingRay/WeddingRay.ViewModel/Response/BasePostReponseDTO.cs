﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeddingRay.ViewModel.Response
{
    public abstract record BasePostReponseDTO
    {
        public long Id { get; init; } = 0;
        public bool Success { get; init; } = true;
        public string Message { get; init; } = string.Empty;
    }
}
